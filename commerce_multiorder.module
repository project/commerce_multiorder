<?php

/**
 * @file
 * Contains commerce_multiorder.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Form\ViewsForm;

/**
 * Implements hook_help().
 */
function commerce_multiorder_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the commerce_multiorder module.
    case 'help.page.commerce_multiorder':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allow Multiorder Capability') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_alter().
 */
function commerce_multiorder_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_state->getFormObject() instanceof ViewsForm) {
    /** @var \Drupal\views\ViewExecutable $view */
    $view = reset($form_state->getBuildInfo()['args']);
    // Only add the Checkout button if the cart form view has order items.
    if ($view->storage->get('tag') == 'commerce_cart_form' && !empty($view->result)) {
      $form['actions']['checkout']['#submit'] = array_merge($form['#submit'], ['commerce_multiorder_order_item_views_form_submit']);

    }
  }
}

/**
 * Submit handler used to redirect to the checkout page.
 */
function commerce_multiorder_order_item_views_form_submit($form, FormStateInterface $form_state) {
  $carts = \Drupal::service('commerce_cart.cart_provider')->getCarts();
  $carts = array_filter($carts, function ($cart) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    return $cart->hasItems();
  });
  $cartIds = array_map(function ($cart) {
    return $cart->id();
  }, $carts);
  $form_state->setRedirect('commerce_multiorder_checkout.form', [
    'step' => 'order_information',
    'ids' => implode('-', $cartIds),
  ]);
}

/**
 * Implements hook_views_data_alter().
 *
 * @param array $data
 */
function commerce_multiorder_views_data_alter(array &$data) {
  $data['commerce_order_item']['commerce_multiorder_item_edit_quantity']['field'] = [
    'title' => t('Commerce Multiorder : Edit Quantity'),
    'help' => '',
    'id' => 'commerce_multiorder_item_edit_quantity',
  ];
}

/**
 * Implementrs hook_views_data(
 */
function commerce_multiorder_views_data() {
  $data['views']['commerce_multiorder_commerce_order_total'] = [
    'title' => t('Commerce Multiorder checkout: Orders total'),
    'help' => t('Displays the orders total field, requires an Order ID argument.'),
    'area' => [
      'id' => 'commerce_multiorder_commerce_order_total',
    ],
  ];
  return $data;
}
