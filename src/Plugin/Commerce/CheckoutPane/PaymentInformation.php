<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentInformation as BasePaymentInformation;
use Drupal\Core\Form\FormStateInterface;

/**
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_payment_information",
 *   label = @Translation("Commerce Multiorder Payment information"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class PaymentInformation extends BasePaymentInformation implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->hasOrderAttached()) {
      foreach ($this->getOrderAttached() as $order) {
        $order->setBillingProfile($this->order->getBillingProfile());
        $order->set('payment_gateway', $this->order->payment_gateway->entity);
        $order->set('payment_method', $this->order->payment_method->entity);
        $order->save();
      }
    }
  }
}
