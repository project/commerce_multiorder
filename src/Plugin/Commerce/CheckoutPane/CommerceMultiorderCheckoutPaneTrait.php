<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_multiorder\Event\CommerceMultiorderPaneEvent;
use Drupal\commerce_multiorder\Event\CommerceMultiorderPaneSummaryEvent;
use Drupal\Core\Form\FormStateInterface;

trait CommerceMultiorderCheckoutPaneTrait {

  /**
   * @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface
   */
  protected $checkoutFlow;

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   */
  public function getOrderAttached(): array {
    return $this->checkoutFlow->getOrderAttached();
  }

  /**
   * Return true if the url contains an additional order id
   *
   * @return bool
   */
  public function hasOrderAttached(): bool {
    $order = $this->checkoutFlow->getOrderAttached();
    return !empty($order);
  }

  /**
   * @inheritDoc
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);
    $event = new CommerceMultiorderPaneEvent($this->pluginId, $pane_form);
    /**
     * @var CommerceMultiorderPaneEvent $event
     */
    \Drupal::service('event_dispatcher')
      ->dispatch(CommerceMultiorderPaneEvent::COMMERCE_MULTIORDER_PANE_FORM_ALTER, $event);
    return $event->getPaneForm();
  }

  /**
   * @inheritDoc
   */
  public function buildPaneSummary() {
    $summary = parent::buildPaneSummary();
    $event = new CommerceMultiorderPaneSummaryEvent($this, $summary);
    \Drupal::service('event_dispatcher')
      ->dispatch(CommerceMultiorderPaneSummaryEvent::COMMERCE_MULTIORDER_PANE_SUMMARY, $event);
    return $event->getSummary();
  }
}
