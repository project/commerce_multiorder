<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CompletionMessage as BaseCompletionMessage;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the completion message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_completion_message",
 *   label = @Translation("Commerce Multiorder Completion message"),
 *   default_step = "complete",
 * )
 */
class CompletionMessage extends BaseCompletionMessage implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // TODO: Implement submitOrderAttached() method.
  }
}
