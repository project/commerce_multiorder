<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Review as BaseReview;
use Drupal\commerce_multiorder\Event\CommerceMultiorderPaneEvent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Provides the review pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_review",
 *   label = @Translation("Commerce Multiorder Commerce Multiorder Review"),
 *   default_step = "review",
 * )
 */
class Review extends BaseReview implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface[] $enabled_panes */
    $enabled_panes = array_filter($this->checkoutFlow->getPanes(), function ($pane) {
      return !in_array($pane->getStepId(), ['_sidebar', '_disabled']);
    });
    foreach ($enabled_panes as $pane_id => $pane) {
      if ($summary = $pane->buildPaneSummary()) {
        if ($summary && !is_array($summary)) {
          $summary = [
            '#markup' => $summary,
          ];
        }

        $label = isset($summary['#title']) ? $summary['#title'] : $pane->getDisplayLabel();
        if ($pane->isVisible()) {
          $edit_link = Link::createFromRoute($this->t('Edit'), 'commerce_multiorder_checkout.form', [
            'ids' => implode('-', $this->checkoutFlow->getAllOrdersId()),
            'step' => $pane->getStepId(),
          ]);
          $label .= ' (' . $edit_link->toString() . ')';
        }
        $pane_form[$pane_id] = [
          '#type' => 'fieldset',
          '#title' => $label,
        ];
        $pane_form[$pane_id]['summary'] = $summary;
      }
    }
    $event = new CommerceMultiorderPaneEvent($this->pluginId, $pane_form);
    /**
     * @var CommerceMultiorderPaneEvent $event
     */
    \Drupal::service('event_dispatcher')
      ->dispatch(CommerceMultiorderPaneEvent::COMMERCE_MULTIORDER_PANE_FORM_ALTER, $event);
    return $event->getPaneForm();
  }

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // TODO: Implement submitOrderAttached() method.
  }
}
