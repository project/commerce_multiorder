<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_multiorder\CommerceMultiorderPaymentInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess as BasePaymentProcess;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the payment process pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_payment_process",
 *   label = @Translation("Commerce Multiorder Payment process"),
 *   default_step = "payment",
 *   wrapper_element = "container",
 * )
 */
class PaymentProcess extends BasePaymentProcess implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  protected $messenger;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface|NULL $checkout_flow
   *
   * @return \Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * @param array $pane_form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $complete_form
   *
   * @return array|void
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $error_step_id = $this->getErrorStepId();
    // The payment gateway is currently always required to be set.
    if ($this->order->get('payment_gateway')->isEmpty()) {
      $this->messenger()->addError($this->t('No payment gateway selected.'));
      $this->checkoutFlow->redirectToStep($error_step_id);
    }
    if ($this->hasOrderAttached()) {
      foreach ($this->getOrderAttached() as $order) {
        if ($order->get('payment_gateway')->isEmpty()) {
          $this->messenger()
            ->addError($this->t('No payment gateway selected.'));
          $this->checkoutFlow->redirectToStep($error_step_id);
        }
      }
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->payment_gateway->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $payment = $this->createPayment($payment_gateway);
    $next_step_id = $this->checkoutFlow->getNextStepId($this->getStepId());

    if ($payment_gateway_plugin instanceof CommerceMultiorderPaymentInterface) {

      try {
        $payment->payment_method = $this->order->payment_method->entity;
        $payments = [$payment];
        if ($this->hasOrderAttached()) {
          foreach ($this->getOrderAttached() as $order) {
            $payment = $this->createOrderAttachedPayment($order);
            $payment->payment_method = $this->order->payment_method->entity;
            $payments[] = $payment;
          }
        }
        $payment_gateway_plugin->multiorderCreatePayment($payments, $this->configuration['capture']);
        $this->checkoutFlow->redirectToStep($next_step_id);
      } catch (DeclineException $e) {
        $message = $this->t('We encountered an error processing your payment method. Please verify your details and try again.');
        $this->messenger()->addError($message);
        $this->checkoutFlow->redirectToStep($error_step_id);
      } catch (PaymentGatewayException $e) {
        $this->logger->error($e->getMessage());
        $message = $this->t('We encountered an unexpected error processing your payment method. Please try again later.');
        $this->messenger()->addError($message);
        $this->checkoutFlow->redirectToStep($error_step_id);
      }
    }
    else {
      $this->logger->critical($this->t('The @class must implements CommerceMultiorderPaymentInterface', ['@class' => get_class($payment_gateway_plugin)]));
      if ($this->hasOrderAttached()) {
        foreach ($this->getOrderAttached() as $order) {
          $order->delete();
        }
      }
      $this->order->delete();
      $this->messenger->addWarning($this->t('The payment Gateway "@gateway" is not supported by Commerce multiorder,
        the payment method must implement the
        CommerceMultiorderPaymentInterface', [
        '@gateway' => $payment_gateway->label(),
      ]));
      throw new NeedsRedirectException(Url::fromUri('internal:/')->toString());
    }
  }

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {

  }

  /**
   * Creates the payment to be processed.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway in use.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The created payment.
   */
  protected function createOrderAttachedPayment(OrderInterface $order) {
    $payment_gateway = $this->order->payment_gateway->entity;
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $order->getBalance(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $order->id(),
    ]);
    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if ($this->order->isPaid() || $this->order->getTotalPrice()->isZero()) {
      // No payment is needed if the order is free or has already been paid.
      return FALSE;
    }
    foreach ($this->getOrderAttached() as $order) {
      if ($order->isPaid() || $order->getTotalPrice()->isZero()) {
        return FALSE;
      }
    }
    $payment_info_pane = $this->checkoutFlow->getPane('commerce_multiorder_payment_information');
    if (!$payment_info_pane->isVisible() || $payment_info_pane->getStepId() == '_disabled') {
      // Hide the pane if the PaymentInformation pane has been disabled.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Gets the step ID that the customer should be sent to on error.
   *
   * @return string
   *   The error step ID.
   */
  protected function getErrorStepId() {
    // Default to the step that contains the PaymentInformation pane.
    $step_id = $this->checkoutFlow->getPane('commerce_multiorder_payment_information')
      ->getStepId();
    if ($step_id == '_disabled') {
      // Can't redirect to the _disabled step. This could mean that isVisible()
      // was overridden to allow PaymentProcess to be used without a
      // payment_information pane, but this method was not modified.
      throw new \RuntimeException('Cannot get the step ID for the payment_information pane. The pane is disabled.');
    }

    return $step_id;
  }
}
