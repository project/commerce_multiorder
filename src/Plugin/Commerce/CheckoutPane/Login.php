<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login as BaseLogin;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the login pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_login",
 *   label = @Translation("Commerce Multiorder Login or continue as guest"),
 *   default_step = "login",
 * )
 */
class Login extends BaseLogin implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->hasOrderAttached()) {
      foreach ($this->getOrderAttached() as $order) {
        $order->setCustomer($this->order->getCustomer())->save();
      }
    }
  }
  protected function canRegisterAfterCheckout() {
    $completion_register_pane = $this->checkoutFlow->getPane('commerce_multiorder_completion_message');
    return $completion_register_pane->getStepId() != '_disabled';
  }
}
