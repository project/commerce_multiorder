<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;

interface CommerceMultiorderPaneInterface {

  /**
   * @return array|null
   */
  public function getOrderAttached(): array;

  /**
   * Submit handler of all attached orders pane
   * @param array $pane_form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $complete_form
   *
   * @return mixed
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form);
}
