<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\ContactInformation as BaseContactInformation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the contact information pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_contact_information",
 *   label = @Translation("Commerce Multiorder Contact information"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class ContactInformation extends BaseContactInformation implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    if ($this->hasOrderAttached()) {
      foreach ($this->getOrderAttached() as $order) {
        $order->setEmail($values['email'])->save();
      }
    }
  }
}
