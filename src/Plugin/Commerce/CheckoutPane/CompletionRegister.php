<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Event\CheckoutCompletionRegisterEvent;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CompletionRegister as BaseCompletionRegister;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the registration after checkout pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_completion_register",
 *   label = @Translation("Commerce Multiorder Guest registration after
 *   checkout"), display_label = @Translation("Account information"),
 *   default_step = "complete",
 * )
 */
class CompletionRegister extends BaseCompletionRegister implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->hasOrderAttached()) {
      foreach ($this->getOrderAttached() as $order) {
        $this->orderAssignment->assign($order, $this->order->getCustomer());
        $event = new CheckoutCompletionRegisterEvent($this->order->getCustomer(), $this->order);
        $this->eventDispatcher->dispatch(CheckoutEvents::COMPLETION_REGISTER, $event);
      }
    }
  }
}
