<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\OrderSummary as BaseOrderSummary;
use Drupal\commerce_multiorder\Event\CommerceMultiorderPaneEvent;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Order summary pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_multiorder_order_summary",
 *   label = @Translation("Commerce Multiorder Order summary"),
 *   default_step = "_sidebar",
 *   wrapper_element = "container",
 * )
 */
class OrderSummary extends BaseOrderSummary implements CommerceMultiorderPaneInterface {

  use CommerceMultiorderCheckoutPaneTrait;

  /**
   * @param array $pane_form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $complete_form
   *
   * @return array
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    if ($this->configuration['view']) {
      $ids = [$this->order->id()];
      if($this->hasOrderAttached()){
        foreach ($this->getOrderAttached() as $order){
          $ids[] = $order->id();
        }
      }
      $pane_form['summary'] = [
        '#type' => 'view',
        '#name' => $this->configuration['view'],
        '#display_id' => 'default',
        '#arguments' => [implode('+', $ids)],
        '#embed' => TRUE,
      ];
    }
    else {

      $pane_form['summary'][] = [
        '#theme' => 'commerce_checkout_order_summary',
        '#order_entity' => $this->order,
        '#checkout_step' => $complete_form['#step_id'],
      ];
      if ($this->hasOrderAttached()) {
        foreach ($this->getOrderAttached() as $order) {
          $pane_form['summary'][] = [
            '#theme' => 'commerce_checkout_order_summary',
            '#order_entity' => $order,
            '#checkout_step' => $complete_form['#step_id'],
          ];
        }
      }
    }
    $event = new CommerceMultiorderPaneEvent($this->pluginId, $pane_form);
    /**
     * @var CommerceMultiorderPaneEvent $event
     */
    \Drupal::service('event_dispatcher')
      ->dispatch(CommerceMultiorderPaneEvent::COMMERCE_MULTIORDER_PANE_FORM_ALTER, $event);
    return $event->getPaneForm();
  }

  /**
   * @inheritDoc
   */
  public function submitOrderAttached(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // TODO: Implement submitOrderAttached() method.
  }
}
