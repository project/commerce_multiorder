<?php

namespace Drupal\commerce_multiorder\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\CheckoutPaneManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;
use Drupal\commerce_multiorder\Event\CommerceMultiorderCheckoutFlowEvent;
use Drupal\commerce_multiorder\Plugin\Commerce\CheckoutPane\CommerceMultiorderPaneInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 *
 * @CommerceCheckoutFlow(
 *   id = "commerce_multiorder_checkoutflow",
 *   label = "Multiorder Checkout",
 * )
 */
class CommerceMultiorderCheckoutFlow extends CheckoutFlowWithPanesBase {

  /**
   * @var \Drupal\Core\Entity\EntityInterface[]|null
   */
  protected $ordersAttached;

  /**
   * @var array
   */
  protected $ids;

  /**
   * CommerceMultiorderCheckoutFlow constructor.
   *
   * @param array $configuration
   * @param $pane_id
   * @param $pane_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\commerce_checkout\CheckoutPaneManager $pane_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration,
                              $pane_id,
                              $pane_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              EventDispatcherInterface $event_dispatcher,
                              RouteMatchInterface $route_match,
                              CheckoutPaneManager $pane_manager) {
    parent::__construct($configuration, $pane_id, $pane_definition, $entity_type_manager, $event_dispatcher, $route_match, $pane_manager);
    if ($route_match->getParameter('ids')) {
      $this->ids = explode('-', $route_match->getParameter('ids'));
      if (!empty($this->ids)) {
        $orders = $this->entityTypeManager->getStorage('commerce_order')
          ->loadMultiple($this->ids);
        if (!empty($orders)) {
          $this->order = array_shift($orders);
          $this->ordersAttached = $orders;
        }
      }
    }
  }

  /**
   * Returns an array containing the list of orders id passed as a route
   * parameter
   *
   * @return array
   */
  public function getAllOrdersId() {
    return $this->ids;
  }

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   */
  public function getOrderAttached() {
    return $this->ordersAttached;
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    $steps = [
        'login' => [
          'label' => $this->t('Login'),
          'previous_label' => $this->t('Go back'),
          'has_sidebar' => FALSE,
        ],
        'order_information' => [
          'label' => $this->t('Order information'),
          'has_sidebar' => TRUE,
          'previous_label' => $this->t('Go back'),
        ],
        'review' => [
          'label' => $this->t('Review'),
          'next_label' => $this->t('Continue to review'),
          'previous_label' => $this->t('Go back'),
          'has_sidebar' => TRUE,
        ],
      ] + parent::getSteps();
    $event = new CommerceMultiorderCheckoutFlowEvent($this->pluginId, $steps);
    $this->eventDispatcher->dispatch(CommerceMultiorderCheckoutFlowEvent::COMMERCE_MULTIORDER_CHECKOUT_FLOW_EVENT,
      $event);
    return $event->getSteps();
  }

  /**
   * Builds the actions element for the current form.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The actions element.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $steps = $this->getVisibleSteps();
    $next_step_id = $this->getNextStepId($form['#step_id']);
    $previous_step_id = $this->getPreviousStepId($form['#step_id']);
    $has_next_step = $next_step_id && isset($steps[$next_step_id]['next_label']);
    $has_previous_step = $previous_step_id && isset($steps[$previous_step_id]['previous_label']);

    $actions = [
      '#type' => 'actions',
      '#access' => $has_next_step,
    ];
    if ($has_next_step) {
      $actions['next'] = [
        '#type' => 'submit',
        '#value' => $steps[$next_step_id]['next_label'],
        '#button_type' => 'primary',
        '#submit' => ['::submitForm'],
      ];
      if ($has_previous_step) {
        $label = $steps[$previous_step_id]['previous_label'];
        $options = [
          'attributes' => [
            'class' => ['link--previous'],
          ],
        ];
        $actions['next']['#suffix'] = Link::createFromRoute($label, 'commerce_multiorder_checkout.form', [
          'ids' => implode('-', $this->ids),
          'step' => $previous_step_id,
        ], $options)->toString();
      }
    }

    return $actions;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    foreach ($this->getVisiblePanes($form['#step_id']) as $pane_id => $pane) {
      $pane->submitOrderAttached($form[$pane_id], $form_state, $form);
    }
    if ($this->hasSidebar($form['#step_id'])) {
      foreach ($this->getVisiblePanes('_sidebar') as $pane_id => $pane) {
        $pane->submitOrderAttached($form['sidebar'][$pane_id], $form_state, $form);
      }
    }

    if ($next_step_id = $this->getNextStepId($form['#step_id'])) {
      $this->onStepChangeOrderAttached($next_step_id);
      $form_state->setRedirect('commerce_multiorder_checkout.form', [
        'step' => $next_step_id,
        'ids' => implode('-', $this->ids),
      ]);
    }
  }

  /**
   * @param $step_id
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function onStepChangeOrderAttached($step_id) {
    if (!empty($this->ordersAttached)) {
      foreach ($this->ordersAttached as $order) {
        // Lock the order while on the 'payment' checkout step. Unlock elsewhere.
        if ($step_id == 'payment') {
          $order->lock();
        }
        elseif ($step_id != 'payment') {
          $order->unlock();
        }
        // Place the order.
        if ($step_id == 'complete' && $order->getState()
            ->getId() == 'draft') {
          $order->getState()->applyTransitionById('place');
        }
        $order->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function redirectToStep($step_id) {
    $available_step_ids = array_keys($this->getVisibleSteps());
    if (!in_array($step_id, $available_step_ids)) {
      throw new \InvalidArgumentException(sprintf('Invalid step ID "%s" passed to redirectToStep().', $step_id));
    }

    $this->order->set('checkout_step', $step_id);
    $this->onStepChange($step_id);
    $this->order->save();
    if (!empty($this->ordersAttached)) {
      foreach ($this->ordersAttached as $order) {
        $order->set('checkout_step', $step_id);
        if ($step_id == 'payment') {
          $order->lock();
        }
        elseif ($step_id != 'payment') {
          $order->unlock();
        }
        // Place the order.
        if ($step_id == 'complete' && $order->getState()->getId() == 'draft') {
          $order->getState()->applyTransitionById('place');
        }
        $order->save();
      }
    }
    throw new NeedsRedirectException(Url::fromRoute('commerce_multiorder_checkout.form', [
      'ids' => implode('-', $this->ids),
      'step' => $step_id,
    ])->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function getPanes() {
    $panes = parent::getPanes();
    $panes = array_filter($panes, function ($pane) {
      return is_subclass_of($pane, CommerceMultiorderPaneInterface::class);
    });
    return $panes;
  }
}
