<?php

namespace Drupal\commerce_multiorder\Plugin\views\area;

use Drupal\commerce_multiorder\Event\CommerceMultiorderTotalViewsEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Plugin\views\area\OrderTotal as BaseOrderTotal;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines an order total area handler.
 *
 * Shows the order total field with its components listed in the footer of a
 * View.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("commerce_multiorder_commerce_order_total")
 */
class CommerceMultiorderOrderTotal extends BaseOrderTotal {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityManger;

  /**
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->entityManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {

      foreach ($this->view->argument as $name => $argument) {
        // First look for an order_id argument.
        if (!$argument instanceof NumericArgument) {
          continue;
        }
        if (!in_array($argument->getField(), [
          'commerce_order.order_id',
          'commerce_order_item.order_id',
        ])) {
          continue;
        }
        if ($orders = $this->orderStorage->loadMultiple(explode('+', $argument->getValue()))) {
          $event = new CommerceMultiorderTotalViewsEvent($orders, $this->view);
          $this->eventDispatcher->dispatch(CommerceMultiorderTotalViewsEvent::COMMERCE_MULTORDER_TOTAL_VIEWS_OVERRIDE_OUTPUT, $event);
          if ($event->hasOutput()) {
            return $event->getOutput();
          }
          $total = [];
          foreach ($orders as $order) {
            $total[] = $order->get('total_price')->view([
              'label' => 'hidden',
              'type' => 'commerce_order_total_summary',
            ]);
          }
          return $total;
        }
      }
    }
    return [];
  }
}
