<?php

namespace Drupal\commerce_multiorder\Plugin\views\field;

use Drupal\commerce_cart\Plugin\views\field\EditQuantity;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_multiorder_item_edit_quantity")
 */
class CommerceMultiorderEditQuantity extends EditQuantity {

  /**
   * {@inheritdoc}
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($row);
      if ($this->options['allow_decimal']) {
        $form_display = commerce_get_entity_display('commerce_order_item', $order_item->bundle(), 'form');
        $quantity_component = $form_display->getComponent('quantity');
        $step = $quantity_component['settings']['step'];
        $precision = $step >= '1' ? 0 : strlen($step) - 2;
      }
      else {
        $step = 1;
        $precision = 0;
      }

      $form[$this->options['id']][$row_index] = [
        '#type' => 'number',
        '#title' => $this->t('Quantity'),
        '#title_display' => 'invisible',
        '#default_value' => round($order_item->getQuantity(), $precision),
        '#size' => 4,
        '#min' => 0,
        '#max' => 9999,
        '#step' => $step,
        '#required' => TRUE,
      ];
    }
    $form['actions']['submit']['#update_cart'] = TRUE;
    $form['actions']['submit']['#show_update_message'] = TRUE;
    $form['actions']['submit']['#value'] = $this->t('Update cart');
  }

  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (empty($triggering_element['#update_cart'])) {
      return;
    }

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $oids = explode('+', $this->view->argument['order_id']->getValue());
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    foreach ($oids as $oid) {
      $cart = $order_storage->load($oid);
      $quantities = $form_state->getValue($this->options['id'], []);
      $save_cart = FALSE;
      foreach ($quantities as $row_index => $quantity) {
        if (!is_numeric($quantity) || $quantity < 0) {
          continue;
        }
        /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
        $order_item = $this->getEntity($this->view->result[$row_index]);
        if ($order_item->getQuantity() == $quantity) {
          // The quantity hasn't changed.
          continue;
        }

        if ($quantity > 0) {
          $order_item->setQuantity($quantity);
          $this->cartManager->updateOrderItem($cart, $order_item, FALSE);
        }
        else {
          // Treat quantity "0" as a request for deletion.
          $this->cartManager->removeOrderItem($cart, $order_item, FALSE);
        }
        $save_cart = TRUE;
      }

      if ($save_cart) {
        $cart->save();
        if (!empty($triggering_element['#show_update_message'])) {
          $this->messenger->addMessage($this->t('Your shopping cart has been updated.'));
        }
      }
    }
  }

}
