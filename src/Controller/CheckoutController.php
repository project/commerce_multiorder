<?php

namespace Drupal\commerce_multiorder\Controller;

use Drupal\commerce_cart\CartSession;
use Drupal\commerce_checkout\Controller\CheckoutController as BaseCheckoutController;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the checkout form page.
 */
class CheckoutController extends BaseCheckoutController {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\commerce_checkout\Controller\CheckoutController
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Checks access for the form page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account) {
    // parent::checkAccess($route_match, $account);
    $ids = explode('-', $route_match->getParameter('ids'));
    foreach ($ids as $id) {
      $order = $this->entityTypeManager->getStorage('commerce_order')
        ->load($id);
      if (!$order) {
        return AccessResult::forbidden();
      }
      if ($order->getState()->value == 'canceled') {
        return AccessResult::forbidden()->addCacheableDependency($order);
      }
      // The user can checkout only their own non-empty orders.
      if ($account->isAuthenticated()) {
        $customer_check = $account->id() == $order->getCustomerId();
      }
      else {
        $active_cart = $this->cartSession->hasCartId($order->id(), CartSession::ACTIVE);
        $completed_cart = $this->cartSession->hasCartId($order->id(), CartSession::COMPLETED);
        $customer_check = $active_cart || $completed_cart;
      }

      $access = AccessResult::allowedIf($customer_check)
        ->andIf(AccessResult::allowedIf($order->hasItems()))
        ->andIf(AccessResult::allowedIfHasPermission($account, 'access checkout'))
        ->addCacheableDependency($order);
      if (!$access->isAllowed()) {
        return $access;
      }

    }
    return AccessResult::allowed();
  }

  public function formPage(RouteMatchInterface $route_match) {
    $ids = explode('-', $route_match->getParameter('ids'));
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $orders = $this->entityTypeManager->getStorage('commerce_order')
      ->loadMultiple($ids);
    $order = array_shift($orders);
    $requested_step_id = $route_match->getParameter('step');

    $step_id = $this->checkoutOrderManager->getCheckoutStepId($order, $requested_step_id);
    if ($requested_step_id != $step_id) {
      $url = Url::fromRoute('commerce_multiorder_checkout.form', [
        'ids' => $route_match->getParameter('ids'),
        'step' => $step_id,
      ]);
      return new RedirectResponse($url->toString());
    }
    $checkout_flow = $this->checkoutOrderManager->getCheckoutFlow($order);
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    return $this->formBuilder->getForm($checkout_flow_plugin, $step_id);
  }

}
