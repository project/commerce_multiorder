<?php

namespace Drupal\commerce_multiorder\Controller;

use Drupal\commerce_cart\Controller\CartController as BaseCartController;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Provides the cart page.
 */
class CartController extends BaseCartController {

  /**
   * Outputs a cart view for each non-empty cart belonging to the current user.
   *
   * @return array
   *   A render array.
   */
  public function cartPage() {
    $build = [];
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->addCacheContexts(['user', 'session']);

    $carts = $this->cartProvider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      return $cart->hasItems();
    });
    if (!empty($carts)) {
      $ids = [];
      foreach ($carts as $cart) {
        $ids[] = $cart->id();
        $cart_id = $cart->id();
      }
      $cart_views = $this->getCartViews($carts);
      $build[0] = [
        '#prefix' => '<div class="cart cart-form">',
        '#suffix' => '</div>',
        '#type' => 'view',
        '#name' => $cart_views[$cart_id],
        '#display_id' => "default",
        '#arguments' => [implode('+', $ids)],
        '#embed' => TRUE,
      ];
    }
    else {
      $build['empty'] = [
        '#theme' => 'commerce_cart_empty_page',
      ];
    }
    $build['#cache'] = [
      'max-age' => 0,
    ];
    return $build;
  }
}
