<?php

namespace Drupal\commerce_multiorder\Event;

use Drupal\views\ViewExecutable;
use Symfony\Component\EventDispatcher\Event;

class CommerceMultiorderTotalViewsEvent extends Event {

  /**
   * @var \Drupal\commerce_order\Entity\OrderInterface[]
   */
  protected $orders;

  /**
   * @var \Drupal\views\ViewExecutable
   */
  protected $views;

  /**
   * @var array|string|bool
   */
  protected $output;

  const COMMERCE_MULTORDER_TOTAL_VIEWS_OVERRIDE_OUTPUT = 'commerce_multiorder.total_views.override_output';

  /**
   * CommerceMultiorderTotalViewsEvent constructor.
   *
   * @param $orders
   * @param \Drupal\views\ViewExecutable $views
   */
  public function __construct($orders, ViewExecutable $views) {
    $this->orders = $orders;
    $this->views = $views;
  }

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface[]
   */
  public function getOrders(): array {
    return $this->orders;
  }

  /**
   * @param array $orders
   *
   * @return $this
   */
  public function setOrders(array $orders) {
    $this->orders = $orders;
    return $this;
  }

  /**
   * @return \Drupal\views\ViewExecutable
   */
  public function getViews() {
    return $this->views;
  }

  /**
   * @param \Drupal\views\ViewExecutable $views
   *
   * @return $this
   */
  public function setViews(ViewExecutable $views) {
    $this->views = $views;
    return $this;
  }

  /**
   * @return array|bool|string
   */
  public function getOutput() {
    return $this->output;
  }

  /**
   * Return true if the output is not empty
   *
   * @return bool
   */
  public function hasOutput() {
    return !empty($this->output);
  }

  /**
   * @param mixed $output
   */
  public function setOutput($output) {
    $this->output = $output;
  }
}
