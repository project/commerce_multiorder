<?php

namespace Drupal\commerce_multiorder\Event;

use Symfony\Component\EventDispatcher\Event;

class CommerceMultiorderCheckoutFlowEvent extends Event {

  const COMMERCE_MULTIORDER_CHECKOUT_FLOW_EVENT = 'commerce_multiorder_checkout_flow.alter_step';

  /**
   * @var array
   */
  protected $steps;

  /**
   * @var string
   */
  protected $îd;

  /**
   * CommerceMultiorderCheckoutFlowEvent constructor.
   *
   * @param array $steps
   */
  public function __construct($id, array $steps) {
    $this->steps = $steps;
    $this->îd = $id;
  }

  /**
   * @return array
   */
  public function getSteps(): array {
    return $this->steps;
  }

  /**
   * @return string
   */
  public function getÎd(): string {
    return $this->îd;
  }

  /**
   * @param array $steps
   *
   * @return $this
   */
  public function setSteps(array $steps) {
    $this->steps = $steps;
    return $this;
  }

}
