<?php

namespace Drupal\commerce_multiorder\Event;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Symfony\Component\EventDispatcher\Event;

class CommerceMultiorderPaneSummaryEvent extends Event {

  const COMMERCE_MULTIORDER_PANE_SUMMARY = 'commerce_multiorder_pane.summary';

  /**
   * @var CheckoutPaneInterface
   */
  protected $checkoutPane;

  protected $summary;

  /**
   * CommerceMultiorderPaneSummaryEvent constructor.
   *
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface $checkoutPane
   */
  public function __construct(CheckoutPaneInterface $checkoutPane, $summary) {
    $this->checkoutPane = $checkoutPane;
    $this->summary = $summary;
  }

  /**
   * @return CheckoutPaneInterface
   */
  public function getCheckoutPane(): CheckoutPaneInterface {
    return $this->checkoutPane;
  }

  /**
   * @return mixed
   */
  public function getSummary() {
    return $this->summary;
  }

  /**
   * @param mixed $summary
   *
   * @return $this
   */
  public function setSummary($summary) {
    $this->summary = $summary;
    return $this;
  }
}
