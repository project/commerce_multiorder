<?php

namespace Drupal\commerce_multiorder\Event;

use Symfony\Component\EventDispatcher\Event;

class CommerceMultiorderPaneEvent extends Event {

  const COMMERCE_MULTIORDER_PANE_FORM_ALTER = 'commerce_multiorder_pane.form_alter';

  /**
   * @var string
   */
  protected $pane_id;

  /**
   * @var array
   */
  protected $pane_form;

  /**
   * CommerceMultiorderPaneEvent constructor.
   *
   * @param string $pane_id
   * @param array $pane_form
   */
  public function __construct($pane_id, $pane_form) {
    $this->pane_id = $pane_id;
    $this->pane_form = $pane_form;
  }

  /**
   * @return string
   */
  public function getPaneId(): string {
    return $this->pane_id;
  }

  /**
   * @param string $pane_id
   *
   * @return $this
   */
  public function setPaneId(string $pane_id) {
    $this->pane_id = $pane_id;
    return $this;
  }

  /**
   * @return array
   */
  public function getPaneForm(): array {
    return $this->pane_form;
  }

  /**
   * @param array $pane_form
   *
   * @return $this
   */
  public function setPaneForm(array $pane_form) {
    $this->pane_form = $pane_form;
    return $this;
  }

}
