<?php

namespace Drupal\commerce_multiorder;


use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;

interface CommerceMultiorderPaymentInterface extends OnsitePaymentGatewayInterface {

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $payments
   * @param $capture
   *
   * @return mixed
   */
  public function multiorderCreatePayment($payments, $capture);
}
