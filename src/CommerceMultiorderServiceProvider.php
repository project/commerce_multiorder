<?php

namespace Drupal\commerce_multiorder;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class CommerceMultiorderServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    // Overrides language_manager class to test domain language negotiation.
    // Adds entity_type.manager service as an additional argument.
    $definition = $container->getDefinition('commerce_checkout.checkout_order_manager');
    $definition->setClass('Drupal\commerce_multiorder\Alter\CommerceMultiorderCheckoutOrderManager');
  }
}
