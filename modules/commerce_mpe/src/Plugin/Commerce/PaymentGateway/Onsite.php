<?php

namespace Drupal\commerce_mpe\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_multiorder\CommerceMultiorderPaymentInterface;
use Drupal\commerce_payment_example\Plugin\Commerce\PaymentGateway\Onsite as BaseOnsite;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "example_mpe",
 *   label = "Example Multiorder Payment",
 *   display_label = "Example Multiorder Payment",
 *   forms = {
 *     "add-payment-method" =
 *   "Drupal\commerce_payment_example\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" =
 *   "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard",
 *   "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Onsite extends BaseOnsite implements CommerceMultiorderPaymentInterface {


  /**
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $payments
   * @param $capture
   *
   * @return mixed
   */
  public function multiorderCreatePayment($payments, $capture) {
    foreach ($payments as $payment) {
      $this->createPayment($payment);
    }
  }
}
