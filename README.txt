CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module gives the possibility of processing orders in marketplace which will
be processed by Mangopay, Stripe or other. It offers the implementation of a new
type of purchasing tunnel automatically integrated into the order entity Standard
payment methods will not be taken into account, you will have to overload/create
your payment method by inheriting from CommerceMultiorderPaymentInterface.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_multiorder

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_multiorder


REQUIREMENTS
------------

 * Drupal Commerce (https://www.drupal.org/project/commerce):
   Drupal Commerce is used to build eCommerce websites and applications of all
   sizes. At its core it is lean and mean, enforcing strict development standards
   and leveraging the greatest features of Drupal 7 and major modules like Views
   and Rules for maximum flexibility.


INSTALLATION
------------

 * Use `composer require drupal/commerce_multiorder` to install.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * sofiene chaari (sofiene.chaari) - https://www.drupal.org/u/sofienechaari
